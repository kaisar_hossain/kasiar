<?php

namespace Database\Factories;

use App\Models\Student;
use Illuminate\Database\Eloquent\Factories\Factory;

class StudentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Student::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            
            'student_id' => $this->faker->NumberBetween(1001,10000),
            'student_name' => $this->faker->randomElement(array('nicolas','nisham','raina','kabir')),
            'student_email' => $this->faker->email,
            'student_address' => $this->faker->address,
            'student_phone' => $this->faker->phoneNumber,
            'student_photo' => $this->faker->randomElement(array('student\kaisar.jpg','student\kaisarwh.jpg','student\kaisarwh1.jpg')),
        ];
    }
}
