<?php

use Illuminate\Support\Facades\Route;
use App\Http\controllers\PostController;
use App\Http\controllers\StudentTableController;
use App\Http\controllers\StudentEditController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get("post",[PostController::class, 'getpost']);
Route::get("posts",[PostController::class,'getApiData']);

Route::get("getuser",[PostController::class,'Getuser']);

Route::view("users","user")->middleware('cheakAge');
Route::view("products","product");

//group middleware..........
/*Route::group(['middleware'=>['CheakAge']],function(){
    Route::view("users","user");
    Route::view("products","product");
});*/
////-------------


Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('students',[StudentTableController::class,'index']);
Route::get('all_students',[StudentEditController::class,'show_students']);
Route::get('allstudents', [StudentEditController::class,'get_all_students'])->name('all.students');
// Route::group('allstudents', 'StudentEditController', [
//     'get_all_students'  => 'allstudents.data',
//     'show_students' => 'allstudents',
// ]);
Route::get('students/{id}/edit',[StudentEditController::class,'single_students_info'])->name('students.edit');
Route::post('students/store',[StudentEditController::class,'update_student'])->name('students.store');
Route::get('students/delete/{id}',[StudentEditController::class,'destroy_student'])->name('students.delete');
