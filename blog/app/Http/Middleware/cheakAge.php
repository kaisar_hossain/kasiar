<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class cheakAge
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        //echo"<h1>hello i am from outer of condition middleware</h1>";
        if($request->name && $request->name == 'shakib'){
            echo"<h1>hello i am from inner of condition middleware</h1>";
            return redirect('user');
        }
        return $next($request);
    }
}
