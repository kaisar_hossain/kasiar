<?php

namespace App\Http\Controllers;

use App\Models\Teacher;
use Illuminate\Http\Request;

class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $teacher=Teacher::all();
        return response()->json($teacher);
    }

    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $teacher_details= new Teacher;
        $teacher_details->teacher_id = $request->teacher_id;
        $teacher_details->teacher_name = $request->teacher_name;
        $teacher_details->teacher_email = $request->teacher_email;
        $teacher_details->teacher_address = $request->teacher_address;
        $teacher_details->teacher_phone = $request->teacher_phone;
        $teacher_details->teacher_photo = $request->teacher_photo;
        $teacher_details->save();
        // $teacher->save();
        return response('data inserted!!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function show(Teacher $teacher)
    {
        //
        $teacher=Teacher::find($teacher);
        return response()->json($teacher);


    }

   
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $id)
    {
        //
        $teacher=Teacher::findorfail($id);
        $teacher->update($request->all());
        return response("updated");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $teacher=Teacher::findorfail($id);
        $img=$teacher->teacher_photo;
        unlink($img);
        $teacher->delete();
        return response('deleted');
    }
}
