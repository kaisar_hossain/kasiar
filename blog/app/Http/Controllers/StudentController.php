<?php

namespace App\Http\Controllers;

use App\Models\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $student=Student::all();
        return view('user')->with('collection',$student);
    }

    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $student_details = new Student;
        $student_details->student_id = $request->student_id;
        $student_details->student_name = $request->student_name;
        $student_details->student_email = $request->student_email;
        $student_details->student_address = $request->student_address;
        $student_details->student_phone = $request->student_phone;
        $student_details->student_photo = $request->student_photo;
        $student_details->save();
        return response('data inserted');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function show(Student $student)
    {
        $data=Student::find($student);
        return response()->json($data);
    }

    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        $input=$request->all();
        $data=Student::findorfail($id);
        $data->update($input);
        return response('updated');
        
    }

    /**
     * Remove the specified resource from storage.
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
        $data=Student::findorfail($id);
        $img=$data->student_photo;
        unlink($img);
        DB::table('students')
        ->where('id','=',$id)->delete();
        return response($img);
    }
}
