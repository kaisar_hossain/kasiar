<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use Yajra\Datatables\Datatables;
use App\DataTables\StudentDataTable;
use DB;
// use Auth;
// use Str;
use App\Models\Student;
class StudentEditController extends Controller
{
public function show_students(){
	return view('user');
}
public function get_all_students(){
	$query = DB::table('students');
return DataTables::of($query)
->addColumn('action', function($data){
$button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$data->student_id.'" 
data-original-title="edit" class="edit btn btn-success btn-sm edit-post"><i class="far fa-edit">Edit</i></a>';
$button .= '&nbsp;&nbsp;';
$button .= '<a href="javascript:void(0)" data-toggle="tooltip" name="delete" data-id="'.$data->student_id.'" 
class="delete btn btn-danger confirmDelete" id="delete"><i class="fa fa-trash">Delete</i></a>';
return $button;
})
->rawColumns(['action'])
->toJson();
}
public function destroy_student($id){
	$select_student=DB::table('students')->where('student_id','=',$id)->first();
	// dd($select_student) ;
   // $logo_destination = 'frontend_upload_asset/student/logo/';
   // 	$image_destination = 'frontend_upload_asset/student/image/';
   // 	if (file_exists($logo_destination.$select_student->student_logo)) {
   // 		unlink($logo_destination.$select_student->student_logo);
		   
   // 	}
   // 	if (file_exists($image_destination.$select_student->student_image)) {
   // 		unlink($image_destination.$select_student->student_image);
		   
   // 	}
   // return $id;
DB::table('students')
->where('student_id','=',$id)
->delete();
return $id;
}
public function single_students_info($id){
	$data=DB::table('students')->where('student_id','=',$id)->first();

	return response()->json($data);
}
public function store_student(Request $request){
$validatedData = $request->validate([
'student_name' => 'required|max:200|unique:students',
'student_email' => 'required|unique',
'student_address' => 'required|max:50',
]);
	// 	dd($request->all());
// if($request->student_logo==null){
// $logo="student_logo.jpg";

// }
if($request->student_photo==null){
$student="student_image.jpg";

}
		$logo_name = $request->file('student_logo');
		$image_name = $request->file('student_image');
		$logo_destination = 'frontend_upload_asset/student/logo/';
		$image_destination = 'frontend_upload_asset/student/image/';
if ($logo_name) {
$logo = time() . '.' . $logo_name->getClientOriginalExtension();
$logo_name->move($logo_destination, $logo);
}
if ($image_name) {
$student = time() . '.' . $image_name->getClientOriginalExtension();
$image_name->move($image_destination, $student);
}
$data = array('student_name' => $request->student_name,
'slug'=>Str::slug('$request->student_name'),
'student_description' =>$request->student_description,
'student_logo' => $logo,
'student_image' => $student,
);
DB::table('students')->insert($data);
return redirect('Admin/students')->with('message', 'Slider Successfully Added');
}

// ''''student store end here'''''//

public function update_student(Request $request){
	$validatedData = $request->validate([
'student_name' => 'required|max:200',
'student_email' => 'required',
'student_address' => 'required',
]);
//  dd($request->all());

	// $select_student=DB::table('students')->where('student_id','=',$request->student_id)->first();
	

$data = array('student_name' => $request->student_name,
	// 'slug'=>Str::slug('$request->student_name'),

'student_email' => $request->student_email,
'student_address' =>$request->student_address,
'student_phone' => $request->student_phone,

);
DB::table('students')
->where('student_id', $request->student_id)
->update($data);
return redirect('students')->with('message', 'Slider Successfully Added');
}
}