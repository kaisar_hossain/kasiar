<div class="modal fade" id="ajaxModel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modelHeading"></h4>
            </div>
            <div class="modal-body">
                <form name="studentForm" id="studentForm" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="student_id" id="student_id">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Student Name</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="student_name" name="student_name" required="student_name" placeholder="Enter student name" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-sm-2 control-label">Student Email</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="student_email" name="student_email"  placeholder="Enter Student email" required="student_email">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Student Address</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="student_address" name="student_address" required="student_address" placeholder="Enter student_address" >
                        </div>
                    </div>
     
                    <div class="form-group">
                        <label  class="col-sm-2 control-label">Student Phone</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="student_phone" name="student_phone"  placeholder="Enter Student phone" required="student_phone">
                        </div>
                    </div>
                    
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-primary" id="update">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

