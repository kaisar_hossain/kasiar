
@extends('layouts.app');

@section('content')
    <table class="table table-bordered" id="users-table" width="80%">
        <thead>
            <tr>
                <th>student_id</th>
                <th>student_name</th>
                <th>student_email</th>
                <th>student_address</th>
                <th>student_phone</th>
                <th>student_photo</th>
                <th>Created At</th>
                <th>Updated At</th>
                <th>Action</th>
            </tr>
        </thead>
    </table>
    @include('product');
@endsection

@section('script')
<script>
$(document).ready(function () {
    $.ajaxSetup({
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
});

$(function() {
    $('#users-table').DataTable({
        processing: true,
        serverSide: true,
        responsive: false,
        ajax: '{!! route('all.students') !!}',
        columns: [
            { data: 'student_id', name: 'student_id' },
            { data: 'student_name', name: 'student_name' },
            { data: 'student_email', name: 'student_email' },
            { data: 'student_address', name: 'student_address' },
            { data: 'student_phone', name: 'student_phone' },
            { data: 'student_photo', name: 'student_photo', 
                render: function(data, type, full, meta){
                return "<img src={{URL::to('/') }}/" + data + " width='80' class='img-thumbnail' />"
            }, 
                orderable:false
             },
            { data: 'created_at', name: 'created_at' },
            { data: 'updated_at', name: 'updated_at' },
            {data: 'action', name:'action', orderable:false, searchable:false, sClass:'text-center'}
        ]
    });
        
});
$(document).on('click','.edit', function(){
    var id = $(this).data('id');
             $.get('students/' +id+'/edit', function (data) {
            $('#modelHeading').html("Edit data"); //this is title
            
            $('#ajaxModel').modal('show');
            //set the value of each id based on the data obtained from the ajax get request above
            // alert(val(data.id));
            $('#student_id').val(data.student_id);
            $('#student_name').val(data.student_name);
            $('#student_email').val(data.student_email);
            $('#student_address').val(data.student_address);
            $('#student_phone').val(data.student_phone);
            // $('#photo').val(data.student_photo);

    })
});
if ($("#studentForm").length > 0) {
      $("#studentForm").validate({
  
     submitHandler: function(form) {
    
      var actionType = $('#update').val();
      $('#update').html('Sending..');
        $.ajax({
            data: $('#studentForm').serialize(),
            type: "POST",
            url: "{{ route('students.store') }}",
            success: function (data) {
                $('#studentForm').trigger("reset");
                $('#update').html('Save Changes');
                $('#ajaxModel').modal('hide');
                var oTable = $('#users-table').dataTable();
                oTable.fnDraw(false);
            },
            error: function (data) {
                console.log('Error:', data);
                $('#update').html('Save Changes');
            }
        });
     
    }
  })
}
$(document).on('click','#delete',function(){
    var id = $(this).data('id');
    
    if(confirm("Are You sure want to delete !")){
          $.ajax({
              type: "get",
              url: "students/delete/"+id,
              success: function (data) {
                console.log(id);
              var oTable = $('#users-table').dataTable(); 
              oTable.fnDraw(false);
              },
              error: function (data) {
                  console.log('Error:', data);
              }
          });
    }
});
</script>
@endsection

